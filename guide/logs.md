# Log Files

## Print logs

The last 10 logs are saved in json inside **.simp/logs/** . S/O [tslog](https://tslog.js.org/).

Display last logs with the CLI.

```bash
simp logs
```

or

```bash
simp logs --verbose
```

The non-verbose "minimal" output looks actually like that.
And is likely to change for the better.

<p align="center">
  <img class="terminal" src="https://simp.areskul.com/images/logs.png" alt="pretty logs">
</p>

You can inspect logs by pipeline

```bash
simp logs --pipeline production
```

Or by branch

```bash
simp logs --branch master
```

## File tree

Hooks execution generates log files.
Located in .simp/logs/
