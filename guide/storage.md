# Generated Files

## File tree

Every file generated by Simp can be found under

```bash
.simp
├── config
│   └── simp.default.config.mjs
└── logs

```

## Config files

You can store your config files in .simp/config
and import them in your main simp.config.mjs

## Logs

Every log is stored in json [tslog](https://tslog.js.org/).
And it is saved under the log folder under
.simp/logs/
