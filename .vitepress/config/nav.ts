import type { DefaultTheme } from "@/config";

export const nav: DefaultTheme.Config["nav"] = [
  {
    text: "Guide",
    link: "/guide/"
  },
  {
    text: "References",
    link: "/api/"
  },
  {
    text: "Contacts",
    link: "/others/contacts"
  }
];
